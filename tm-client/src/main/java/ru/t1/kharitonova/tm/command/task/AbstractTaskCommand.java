package ru.t1.kharitonova.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.kharitonova.tm.api.endpoint.ITaskEndpoint;
import ru.t1.kharitonova.tm.command.AbstractCommand;
import ru.t1.kharitonova.tm.enumerated.Role;
import ru.t1.kharitonova.tm.enumerated.Status;
import ru.t1.kharitonova.tm.dto.model.TaskDTO;

import java.util.List;

public abstract class AbstractTaskCommand extends AbstractCommand {

    @NotNull
    public ITaskEndpoint getTaskEndpoint() {
        return serviceLocator.getTaskEndpoint();
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    protected void renderTasks(@Nullable final List<TaskDTO> taskDTOS) {
        if (taskDTOS == null || taskDTOS.size() == 0) return;
        int index = 1;
        for (@Nullable final TaskDTO taskDTO : taskDTOS) {
            if (taskDTO == null) continue;
            System.out.println(index + ". " + taskDTO.getName());
            index++;
        }
    }

    protected void showTask(@Nullable final TaskDTO taskDTO) {
        if (taskDTO == null) return;
        System.out.println("ID: " + taskDTO.getId());
        System.out.println("NAME: " + taskDTO.getName());
        System.out.println("DESCRIPTION: " + taskDTO.getDescription());
        System.out.println("STATUS: " + Status.toName(taskDTO.getStatus()));
        System.out.println("PROJECT ID: " + taskDTO.getProjectId());
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}
