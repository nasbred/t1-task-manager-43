package ru.t1.kharitonova.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.kharitonova.tm.api.repository.dto.IProjectDTORepository;
import ru.t1.kharitonova.tm.dto.model.ProjectDTO;
import ru.t1.kharitonova.tm.enumerated.Status;
import ru.t1.kharitonova.tm.exception.field.IdEmptyException;
import ru.t1.kharitonova.tm.exception.field.UserIdEmptyException;

import javax.persistence.EntityManager;
import java.util.Collections;
import java.util.List;

public class ProjectDTORepository extends AbstractUserOwnedDTORepository<ProjectDTO>
        implements IProjectDTORepository {

    public ProjectDTORepository(@NotNull final EntityManager entityManager) {
        super(entityManager);
    }

    @Override
    public void changeStatusById(@Nullable final String id, @Nullable final Status status) {
        entityManager
                .createQuery("UPDATE ProjectDTO e SET status = :status WHERE e.id = :id")
                .setParameter("id", id)
                .setParameter("status", status)
                .executeUpdate();
    }

    @Override
    public boolean existsById(@NotNull final String id) {
        return findOneById(id) != null;
    }

    @Override
    @Nullable
    public ProjectDTO findOneById(@NotNull final String id) {
        if (id.isEmpty()) throw new IdEmptyException();
        @NotNull final String jpql = "SELECT m FROM ProjectDTO m WHERE m.id = :id";
        return entityManager.createQuery(jpql, ProjectDTO.class)
                .setParameter("id", id)
                .setMaxResults(1)
                .getResultList()
                .stream()
                .findFirst()
                .orElse(null);
    }

    @Override
    @Nullable
    public ProjectDTO findOneByIdAndUserId(@NotNull final String userId, @NotNull final String id) {
        if (userId.isEmpty()) throw new UserIdEmptyException();
        if (id.isEmpty()) throw new IdEmptyException();
        @NotNull final String jpql = "SELECT e FROM ProjectDTO e WHERE e.userId = :userId AND e.id = :id";
        return entityManager.createQuery(jpql, ProjectDTO.class)
                .setParameter("userId", userId)
                .setParameter("id", id)
                .setMaxResults(1)
                .getResultList()
                .stream()
                .findFirst()
                .orElse(null);
    }

    @Override
    @Nullable
    public ProjectDTO findOneByIndex(@NotNull final Integer index) {
        @NotNull final String jpql = "SELECT e FROM ProjectDTO e";
        return entityManager.createQuery(jpql, ProjectDTO.class)
                .setMaxResults(index + 1)
                .getResultList()
                .stream()
                .skip(index)
                .findFirst()
                .orElse(null);
    }

    @Override
    @Nullable
    public ProjectDTO findOneByIndexByUserId(@NotNull final String user_id, @NotNull final Integer index) {
        if (user_id.isEmpty()) throw new UserIdEmptyException();
        @NotNull final String jpql = "SELECT e FROM ProjectDTO e WHERE e.userId = :userId";
        return entityManager.createQuery(jpql, ProjectDTO.class)
                .setParameter("userId", user_id)
                .setMaxResults(index + 1)
                .getResultList()
                .stream()
                .skip(index)
                .findFirst()
                .orElse(null);
    }

    @NotNull
    @Override
    public List<ProjectDTO> findAll() {
        return entityManager.createQuery("FROM ProjectDTO", ProjectDTO.class).getResultList();
    }

    @NotNull
    @Override
    public List<ProjectDTO> findAllByUserId(@NotNull final String userId) {
        return entityManager
                .createQuery("FROM ProjectDTO e WHERE e.userId = :userId", ProjectDTO.class)
                .setParameter("userId", userId)
                .getResultList();
    }

    @NotNull
    @Override
    public List<ProjectDTO> findAllByUserIdWithSort(@NotNull final String userId, @NotNull final String sort) {
        if (userId.isEmpty()) return Collections.emptyList();
        @NotNull final String jpql = "SELECT e FROM ProjectDTO e WHERE e.userId = :userId ORDER BY :sort";
        return entityManager.createQuery(jpql, ProjectDTO.class)
                .setParameter("userId", userId)
                .setParameter("sort", sort)
                .getResultList();
    }

    @Override
    public long getSize() {
        return (long) entityManager
                .createQuery("SELECT COUNT(1) FROM ProjectDTO")
                .setMaxResults(1)
                .getSingleResult();
    }

    @Override
    public long getSizeForUser(@NotNull final String user_id) {
        return (long) entityManager
                .createQuery("SELECT COUNT(1) FROM ProjectDTO e WHERE e.userId = :userId")
                .setParameter("userId", user_id)
                .setMaxResults(1)
                .getSingleResult();
    }

    @Override
    public void removeOneById(@NotNull final String id) {
        entityManager
                .createQuery("DELETE FROM ProjectDTO WHERE e.Id = :Id")
                .setParameter("Id", id)
                .executeUpdate();
    }

    @Override
    public void removeOneByIndex(@NotNull final Integer index) {
        entityManager.remove(findOneByIndex(index));
    }

    @Override
    public void removeOneByIndexForUser(@NotNull final String userId, @NotNull final Integer index) {
        entityManager.remove(findOneByIndexByUserId(userId, index));
    }

    @Override
    public void removeAll() {
        entityManager.createQuery("DELETE FROM ProjectDTO").executeUpdate();
    }

    @Override
    public void removeAllByUserId(@NotNull final String userId) {
        entityManager
                .createQuery("DELETE FROM ProjectDTO e WHERE e.userId = :userId")
                .setParameter("userId", userId)
                .executeUpdate();
    }

    @Override
    public void removeAllByList(@NotNull final List<ProjectDTO> models) {
        models.forEach(this::remove);
    }

}
