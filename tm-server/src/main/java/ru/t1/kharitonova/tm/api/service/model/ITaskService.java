package ru.t1.kharitonova.tm.api.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.kharitonova.tm.enumerated.Status;
import ru.t1.kharitonova.tm.enumerated.TaskSort;
import ru.t1.kharitonova.tm.model.Task;

import java.util.List;

public interface ITaskService extends IUserOwnedService<Task> {

    @NotNull
    Task changeTaskStatusByIndex(@Nullable String userId, @Nullable Integer index, @Nullable Status status);

    @NotNull
    Task changeTaskStatusById(@Nullable String userId, @Nullable String id, @Nullable Status status);

    @NotNull
    Task create(@Nullable String userId, @Nullable String name, @Nullable String description);

    long getSize();

    long getSize(@Nullable String userId);

    boolean existsById(@Nullable final String userId, @Nullable final String id);

    @Nullable
    Task findOneById(@Nullable String user_id, @Nullable String id);

    @Nullable
    Task findOneByIndex(@Nullable String userId, @Nullable Integer index);

    @NotNull
    List<Task> findAll();

    @NotNull
    List<Task> findAll(@NotNull String userId);

    @NotNull
    List<Task> findAll(@NotNull String userId, @Nullable TaskSort sort);

    @NotNull
    List<Task> findAllByProjectId(@Nullable String userId, @Nullable String projectId);

    void removeOneById(@Nullable String userId, @Nullable String id);

    void removeOneByIndex(@NotNull String userId, @NotNull Integer index);

    void removeAll();

    void removeAll(@NotNull List<Task> models);

    void removeAllByUserId(@Nullable String userId);

    @NotNull
    Task updateById(
            @Nullable String userId,
            @Nullable String id,
            @Nullable String name,
            @Nullable String description
    );

    @NotNull
    Task updateByIndex(
            @Nullable String userId,
            @Nullable Integer index,
            @Nullable String name,
            @Nullable String description
    );

}
