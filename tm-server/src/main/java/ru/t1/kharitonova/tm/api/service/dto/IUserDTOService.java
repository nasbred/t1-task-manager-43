package ru.t1.kharitonova.tm.api.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.kharitonova.tm.dto.model.UserDTO;
import ru.t1.kharitonova.tm.enumerated.Role;

import java.util.Collection;
import java.util.List;

public interface IUserDTOService extends IAbstractDTOService<UserDTO> {

    @NotNull
    UserDTO create(@Nullable String login, @Nullable String password);

    @NotNull
    UserDTO create(@Nullable String login, @Nullable String password, @Nullable String email);

    @NotNull
    UserDTO create(@Nullable String login, @Nullable String password, @Nullable Role role);

    @NotNull
    List<UserDTO> findAll();

    @Nullable
    UserDTO findById(@Nullable String id);

    @Nullable
    UserDTO findByEmail(@Nullable String email);

    @Nullable
    UserDTO findByLogin(@Nullable String login);

    long getSize();

    @Nullable
    UserDTO removeByLogin(@Nullable String login);

    @Nullable
    UserDTO removeByEmail(@Nullable String email);

    void removeAll();

    UserDTO lockUserByLogin(@Nullable String login);

    UserDTO unlockUserByLogin(@Nullable String login);

    void updateUser(
            @Nullable String id,
            @Nullable String firstName,
            @Nullable String lastName,
            @Nullable String middleName
    );

    @NotNull
    Boolean isLoginExist(@NotNull String login);

    @NotNull
    Boolean isEmailExist(@NotNull String email);

    void set(@NotNull Collection<UserDTO> users);

    void setPassword(@Nullable String id, @Nullable String password);

}
