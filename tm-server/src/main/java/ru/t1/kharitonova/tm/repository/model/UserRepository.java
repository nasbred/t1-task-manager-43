package ru.t1.kharitonova.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.kharitonova.tm.api.repository.model.IUserRepository;
import ru.t1.kharitonova.tm.model.User;

import javax.persistence.EntityManager;

public class UserRepository extends AbstractRepository<User> implements IUserRepository {

    public UserRepository(@NotNull final EntityManager entityManager) {
        super(entityManager);
    }

    @Override
    @Nullable
    public User findOneById(@Nullable final String id) {
        @NotNull final String jpql = "SELECT e FROM User e WHERE e.id = :id";
        return entityManager.createQuery(jpql, User.class)
                .setParameter("id", id)
                .setMaxResults(1)
                .getResultList()
                .stream()
                .findFirst()
                .orElse(null);
    }

    @Override
    @Nullable
    public User findByLogin(@NotNull final String login) {
        @NotNull final String jpql = "SELECT e FROM User e WHERE e.login = :login";
        return entityManager.createQuery(jpql, User.class)
                .setParameter("login", login)
                .setMaxResults(1)
                .getResultList()
                .stream()
                .findFirst()
                .orElse(null);
    }

    @Override
    @Nullable
    public User findByEmail(@NotNull final String email) {
        @NotNull final String jpql = "SELECT e FROM User e WHERE e.email = :email";
        return entityManager.createQuery(jpql, User.class)
                .setParameter("email", email)
                .setMaxResults(1)
                .getResultList()
                .stream()
                .findFirst()
                .orElse(null);
    }

    @Override
    public Long getSize() {
        return (Long) entityManager
                .createQuery("SELECT COUNT(1) FROM User")
                .setMaxResults(1)
                .getSingleResult();
    }

    @Override
    public void setPassword(@Nullable final String id, @NotNull final String password) {
        entityManager
                .createQuery("UPDATE User e SET e.password = :password WHERE e.id = :id")
                .setParameter("id", id)
                .setParameter("password", password)
                .executeUpdate();
    }

    @Override
    public void updateUser(
            @Nullable final String id,
            @Nullable final String firstName,
            @Nullable final String lastName,
            @Nullable final String middleName
    ) {
        @NotNull final String jpql = "UPDATE User e SET e.firstName = :firstName " +
                "e.lastName = :lastName AND e.middleName = :middleName " +
                "WHERE e.id = :id";
        entityManager
                .createQuery(jpql)
                .setParameter("id", id)
                .setParameter("firstName", firstName)
                .setParameter("lastName", lastName)
                .setParameter("middleName", middleName)
                .executeUpdate();
    }

    @Override
    public void lockUserById(@Nullable final String id) {
        @NotNull final String jpql = "UPDATE User e SET e.locked = true WHERE e.id = :id";
        entityManager
                .createQuery(jpql)
                .setParameter("id", id)
                .executeUpdate();
    }

    @Override
    public void unLockUserById(@Nullable final String id) {
        @NotNull final String jpql = "UPDATE User e SET e.locked = false WHERE e.id = :id";
        entityManager
                .createQuery(jpql)
                .setParameter("id", id)
                .executeUpdate();
    }

}
