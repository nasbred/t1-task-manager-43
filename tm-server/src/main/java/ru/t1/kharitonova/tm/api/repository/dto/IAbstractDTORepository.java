package ru.t1.kharitonova.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import ru.t1.kharitonova.tm.dto.model.AbstractModelDTO;

import java.util.Collection;

public interface IAbstractDTORepository<M extends AbstractModelDTO> {

    void add(@NotNull M model);

    void addAll(@NotNull Collection<M> models);

    void update(@NotNull M model);

    void remove(@NotNull M model);

}
