package ru.t1.kharitonova.tm.service.model;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.kharitonova.tm.api.repository.model.IProjectRepository;
import ru.t1.kharitonova.tm.api.repository.model.IUserRepository;
import ru.t1.kharitonova.tm.api.service.IConnectionService;
import ru.t1.kharitonova.tm.api.service.model.IProjectService;
import ru.t1.kharitonova.tm.enumerated.ProjectSort;
import ru.t1.kharitonova.tm.enumerated.Status;
import ru.t1.kharitonova.tm.exception.entity.ModelNotFoundException;
import ru.t1.kharitonova.tm.exception.entity.ProjectNotFoundException;
import ru.t1.kharitonova.tm.exception.field.*;
import ru.t1.kharitonova.tm.exception.user.AccessDeniedException;
import ru.t1.kharitonova.tm.exception.user.UserNotFoundException;
import ru.t1.kharitonova.tm.model.Project;
import ru.t1.kharitonova.tm.model.User;
import ru.t1.kharitonova.tm.repository.model.ProjectRepository;
import ru.t1.kharitonova.tm.repository.model.UserRepository;

import javax.persistence.EntityManager;
import java.util.List;

public final class ProjectService extends AbstractUserOwnedService<Project, IProjectRepository>
        implements IProjectService {

    public ProjectService(@NotNull final IConnectionService connectionService) {
        super(connectionService);
    }

    @NotNull
    @Override
    protected IProjectRepository getRepository(@NotNull final EntityManager entityManager) {
        return new ProjectRepository(entityManager);
    }

    @NotNull
    @Override
    @SneakyThrows
    public Project create(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null) throw new DescriptionEmptyException();

        @NotNull Project project = new Project();
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final IUserRepository userRepository = new UserRepository(entityManager);
        @Nullable final User user = userRepository.findOneById(userId);
        if (user == null) throw new UserNotFoundException();
        project.setName(name);
        project.setDescription(description);
        project.setUser(user);

        try {
            @NotNull final IProjectRepository repository = new ProjectRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.add(project);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return project;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Project changeProjectStatusById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final Status status
    ) {
        @NotNull final EntityManager entityManager = getEntityManager();
        @Nullable Project project = findOneById(userId, id);
        if (project == null) throw new ProjectNotFoundException();
        try {
            @NotNull final IProjectRepository repository = new ProjectRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.changeStatusById(id, status);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return project;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Project changeProjectStatusByIndex(
            @Nullable final String userId,
            @Nullable final Integer index,
            @Nullable final Status status
    ) {
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final IProjectRepository repository = getRepository(entityManager);

        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null || index < 0 || index > getSize(userId))
            throw new IndexIncorrectException();
        if (status == null) throw new StatusIncorrectException();

        @Nullable Project project = findOneByIndex(userId, index);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(status);
        try {
            entityManager.getTransaction().begin();
            repository.update(project);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return project;
    }

    @Override
    @SneakyThrows
    public long getSize() {
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final IProjectRepository repository = getRepository(entityManager);
        try {
            return repository.getSize();
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public long getSize(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final IProjectRepository repository = getRepository(entityManager);
        try {
            return repository.getSize(userId);
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public boolean existsById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final IProjectRepository repository = getRepository(entityManager);
        try {
            return repository.existsById(id);
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public Project findOneById(@Nullable final String user_id, @Nullable final String id) {
        if (user_id == null || user_id.isEmpty()) throw new IdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IProjectRepository repository = getRepository(entityManager);
            return repository.findOneByIdAndUserId(user_id, id);
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public Project findOneByIndex(@Nullable final Integer index) {
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final IProjectRepository repository = getRepository(entityManager);

        if (index == null || index < 0 || index > getSize())
            throw new IndexIncorrectException();
        try {
            return repository.findOneByIndex(index);
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public Project findOneByIndex(@Nullable final String userId, @Nullable final Integer index) {
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final IProjectRepository repository = getRepository(entityManager);

        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null || index < 0 || index > getSize(userId))
            throw new IndexIncorrectException();
        try {
            return repository.findOneByIndexByUserId(userId, index);
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<Project> findAll() {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IProjectRepository repository = getRepository(entityManager);
            return repository.findAll();
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<Project> findAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IProjectRepository repository = getRepository(entityManager);
            return repository.findAllByUserId(userId);
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<Project> findAll(
            @Nullable final String userId,
            @Nullable final ProjectSort sort
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (sort == null) return findAll(userId);
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IProjectRepository repository = getRepository(entityManager);
            return repository.findAllByUserIdWithSort(userId, sort.toString());
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public void removeOneById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IProjectRepository repository = new ProjectRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.findOneByIdAndUserId(userId, id);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public void removeOneByIndex(@Nullable final String userId, @Nullable final Integer index) {
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final IProjectRepository repository = new ProjectRepository(entityManager);
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null || index < 0 || index >= getSize(userId))
            throw new IndexIncorrectException();
        try {
            entityManager.getTransaction().begin();
            repository.removeOneByIndexForUser(userId, index);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public void removeAll() {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IProjectRepository repository = new ProjectRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.removeAll();
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public void removeAll(@Nullable final List<Project> models) {
        if (models == null) throw new ModelNotFoundException();
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IProjectRepository repository = new ProjectRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.removeAllByList(models);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public void removeAllByUserId(@Nullable final String userId) {
        if (userId == null) throw new UserIdEmptyException();
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IProjectRepository repository = new ProjectRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.removeAllByUserId(userId);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public Project updateById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();

        @Nullable final Project project = findOneById(userId, id);
        if (project == null) throw new ProjectNotFoundException();
        project.setName(name);
        project.setDescription(description);

        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IProjectRepository repository = new ProjectRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.update(project);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return project;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Project updateByIndex(
            @Nullable final String userId,
            @Nullable final Integer index,
            @Nullable final String name,
            @Nullable final String description
    ) {
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final IProjectRepository repository = new ProjectRepository(entityManager);

        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        if (index == null || index < 0 || index >= getSize(userId))
            throw new IndexIncorrectException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        @Nullable final Project project = findOneByIndex(userId, index);
        if (project == null) throw new ProjectNotFoundException();
        project.setName(name);
        project.setDescription(description);
        try {
            entityManager.getTransaction().begin();
            repository.update(project);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return project;
    }

}
