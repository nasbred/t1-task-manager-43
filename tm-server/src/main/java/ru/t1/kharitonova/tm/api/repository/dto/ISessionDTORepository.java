package ru.t1.kharitonova.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.kharitonova.tm.dto.model.SessionDTO;

public interface ISessionDTORepository extends IUserOwnedDTORepository<SessionDTO> {

    void removeOneById(@NotNull String id);

    void removeAll();

    void removeAllByUserId(@NotNull String userId);

    @Nullable
    SessionDTO findOneById(@NotNull String id);

    @Nullable
    SessionDTO findOneByUserId(@NotNull String userId);

    long getSize();

}
