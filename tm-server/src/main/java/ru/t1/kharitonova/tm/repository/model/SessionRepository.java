package ru.t1.kharitonova.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.kharitonova.tm.api.repository.model.ISessionRepository;
import ru.t1.kharitonova.tm.exception.field.IdEmptyException;
import ru.t1.kharitonova.tm.exception.field.UserIdEmptyException;
import ru.t1.kharitonova.tm.model.Session;

import javax.persistence.EntityManager;

public class SessionRepository extends AbstractUserOwnedRepository<Session>
        implements ISessionRepository {

    public SessionRepository(@NotNull final EntityManager entityManager) {
        super(entityManager);
    }

    @Override
    @Nullable
    public Session findOneById(@NotNull final String id) {
        if (id.isEmpty()) throw new IdEmptyException();
        @NotNull final String jpql = "SELECT m FROM Session m WHERE m.id = :id";
        return entityManager.createQuery(jpql, Session.class)
                .setParameter("id", id)
                .setMaxResults(1)
                .getResultList()
                .stream()
                .findFirst()
                .orElse(null);
    }

    @Override
    @Nullable
    public Session findOneByUserId(@NotNull final String userId) {
        if (userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final String jpql = "SELECT e FROM Session e WHERE e.userId = :userId";
        return entityManager.createQuery(jpql, Session.class)
                .setParameter("userId", userId)
                .setMaxResults(1)
                .getResultList()
                .stream()
                .findFirst()
                .orElse(null);
    }

    @Override
    public int getSize() {
        return (int) entityManager
                .createQuery("SELECT COUNT(1) FROM Session")
                .setMaxResults(1)
                .getSingleResult();
    }

    @Override
    public void removeOneById(@NotNull final String Id) {
        entityManager
                .createQuery("DELETE FROM Session WHERE e.Id = :Id")
                .setParameter("Id", Id)
                .executeUpdate();
    }

    @Override
    public void removeAll() {
        entityManager.createQuery("DELETE FROM Session").executeUpdate();
    }

    @Override
    public void removeAllByUserId(@NotNull final String userId) {
        entityManager
                .createQuery("DELETE FROM Session e WHERE e.userId = :userId")
                .setParameter("userId", userId)
                .executeUpdate();
    }

}
