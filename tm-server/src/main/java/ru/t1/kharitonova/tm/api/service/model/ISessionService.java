package ru.t1.kharitonova.tm.api.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.kharitonova.tm.model.Session;

public interface ISessionService extends IUserOwnedService<Session> {

    @NotNull
    Session create(@Nullable Session sessionDTO);

    Boolean existsById(@Nullable String id);

    int getSize();

    @Nullable
    Session findOneById(@Nullable final String id);

    @Nullable
    Session findOneByUserId(@Nullable String id);

    void removeById(@Nullable String id);

    void removeAll();

}
