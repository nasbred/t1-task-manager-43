package ru.t1.kharitonova.tm.api.service.dto;

import ru.t1.kharitonova.tm.api.repository.dto.IUserOwnedDTORepository;
import ru.t1.kharitonova.tm.dto.model.AbstractUserOwnedModelDTO;

public interface IUserOwnedDTOService<M extends AbstractUserOwnedModelDTO>
        extends IUserOwnedDTORepository<M>, IAbstractDTOService<M> {
}