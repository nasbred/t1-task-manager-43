package ru.t1.kharitonova.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.kharitonova.tm.api.service.IConnectionService;
import ru.t1.kharitonova.tm.api.service.IPropertyService;
import ru.t1.kharitonova.tm.api.service.dto.IProjectDTOService;
import ru.t1.kharitonova.tm.api.service.dto.ISessionDTOService;
import ru.t1.kharitonova.tm.api.service.dto.ITaskDTOService;
import ru.t1.kharitonova.tm.api.service.dto.IUserDTOService;
import ru.t1.kharitonova.tm.dto.model.SessionDTO;
import ru.t1.kharitonova.tm.dto.model.UserDTO;
import ru.t1.kharitonova.tm.enumerated.Role;
import ru.t1.kharitonova.tm.marker.ServerCategory;
import ru.t1.kharitonova.tm.service.dto.ProjectDTOService;
import ru.t1.kharitonova.tm.service.dto.SessionDTOService;
import ru.t1.kharitonova.tm.service.dto.TaskDTOService;
import ru.t1.kharitonova.tm.service.dto.UserDTOService;

import java.util.ArrayList;
import java.util.List;

@Category(ServerCategory.class)
public class SessionServiceDTOTest {

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    protected final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    private final ISessionDTOService sessionService = new SessionDTOService(connectionService);

    @NotNull
    private final IProjectDTOService projectService = new ProjectDTOService(connectionService);

    @NotNull
    private final ITaskDTOService taskService = new TaskDTOService(connectionService);

    @NotNull
    private final IUserDTOService userService = new UserDTOService(connectionService, propertyService, projectService, taskService);

    @NotNull
    private final List<SessionDTO> sessionDTOList = new ArrayList<>();

    @NotNull
    private List<UserDTO> userDTOList = new ArrayList<>();

    @Before
    public void init() {
        @NotNull final UserDTO userDTOAdmin = new UserDTO();
        userDTOAdmin.setLogin("admin");
        userDTOAdmin.setPasswordHash("admin");
        userDTOAdmin.setEmail("admin@test.ru");
        userDTOAdmin.setRole(Role.ADMIN);

        @NotNull final UserDTO userDTOTest = new UserDTO();
        userDTOTest.setLogin("test");
        userDTOTest.setPasswordHash("test");
        userDTOTest.setEmail("test@test.ru");
        userDTOTest.setRole(Role.USUAL);

        if (userService.findByLogin("admin") == null) {
            userService.add(userDTOAdmin);
            userDTOList.add(userDTOAdmin);
        } else {
            userDTOList.add(userService.findByLogin("admin"));
        }
        if (userService.findByLogin("test") == null) {
            userService.add(userDTOTest);
            userDTOList.add(userDTOTest);
        } else {
            userDTOList.add(userService.findByLogin("test"));
        }

        @Nullable final String adminUserId = userDTOAdmin.getId();
        @Nullable final String testUserId = userDTOTest.getId();
        @NotNull final SessionDTO sessionDTOAdmin = new SessionDTO(adminUserId, Role.ADMIN);
        @NotNull final SessionDTO sessionDTOTest = new SessionDTO(testUserId, Role.USUAL);

        if (sessionService.findOneByUserId(adminUserId) == null) {
            sessionDTOList.add(sessionService.create(sessionDTOAdmin));
        } else {
            sessionDTOList.add(sessionService.findOneByUserId(adminUserId));
        }
        if (sessionService.findOneByUserId(testUserId) == null) {
            sessionDTOList.add(sessionService.create(sessionDTOTest));
        } else {
            sessionDTOList.add(sessionService.findOneByUserId(testUserId));
        }
    }

    @After
    public void clear() {
        sessionService.removeAll();
        userService.removeAll();
        sessionDTOList.clear();
        userDTOList.clear();
    }

    @Test
    public void testCreate() {
        final int expectedSize = sessionDTOList.size();
        Assert.assertEquals(expectedSize, sessionService.getSize());
        @NotNull final SessionDTO sessionDTO = new SessionDTO(userDTOList.get(1).getId(), Role.USUAL);
        sessionService.create(sessionDTO);
        Assert.assertEquals(expectedSize + 1, sessionService.getSize());
    }


    @Test
    public void testExistsByIdPositive() {
        for (@NotNull final SessionDTO sessionDTO : sessionDTOList) {
            @NotNull final String id = sessionDTO.getId();
            Assert.assertTrue(sessionService.existsById(id));
        }
    }

    @Test
    public void testExistsByIdNegative() {
        Assert.assertFalse(sessionService.existsById("Other_Id"));
    }

    @Test
    public void testFindOneByIdPositive() {
        for (@NotNull final SessionDTO sessionDTO : sessionDTOList) {
            @NotNull final String id = sessionDTO.getId();
            @Nullable final SessionDTO sessionDTO1 = sessionService.findOneById(id);
            Assert.assertNotNull(sessionDTO1);
            Assert.assertEquals(id, sessionDTO1.getId());
        }
    }

    @Test
    public void testFindOneByIdNegative() {
        @NotNull final String id = "Other_id";
        @Nullable final SessionDTO sessionDTO = sessionService.findOneById(id);
        Assert.assertNull(sessionDTO);
    }

    @Test
    public void testGetSize() {
        final long projectSize = sessionService.getSize();
        Assert.assertEquals(sessionDTOList.size(), projectSize);
    }

    @Test
    public void testRemoveById() {
        for (int i = 0; i < sessionDTOList.size(); i++) {
            @NotNull final SessionDTO sessionDTO = sessionDTOList.get(i);
            sessionService.removeById(sessionDTO.getId());
            Assert.assertEquals(sessionService.getSize(), sessionDTOList.size() - i - 1);
        }
        Assert.assertEquals(0, sessionService.getSize());
    }

    @Test
    public void testRemoveByIdNegative() {
        @NotNull final String id = "Other_Id";
        sessionService.removeById(id);
        Assert.assertEquals(sessionDTOList.size(), sessionService.getSize());
    }

    @Test
    public void testRemoveAll() {
        sessionService.removeAll();
        Assert.assertEquals(0, sessionService.getSize());
    }

}
