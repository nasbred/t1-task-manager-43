package ru.t1.kharitonova.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.kharitonova.tm.api.service.IConnectionService;
import ru.t1.kharitonova.tm.api.service.IPropertyService;
import ru.t1.kharitonova.tm.api.service.dto.IProjectDTOService;
import ru.t1.kharitonova.tm.api.service.dto.IProjectTaskDTOService;
import ru.t1.kharitonova.tm.api.service.dto.ITaskDTOService;
import ru.t1.kharitonova.tm.api.service.dto.IUserDTOService;
import ru.t1.kharitonova.tm.dto.model.ProjectDTO;
import ru.t1.kharitonova.tm.dto.model.TaskDTO;
import ru.t1.kharitonova.tm.dto.model.UserDTO;
import ru.t1.kharitonova.tm.enumerated.Role;
import ru.t1.kharitonova.tm.enumerated.Status;
import ru.t1.kharitonova.tm.exception.entity.ProjectNotFoundException;
import ru.t1.kharitonova.tm.exception.entity.TaskNotFoundException;
import ru.t1.kharitonova.tm.exception.field.ProjectIdEmptyException;
import ru.t1.kharitonova.tm.exception.field.TaskIdEmptyException;
import ru.t1.kharitonova.tm.exception.field.UserIdEmptyException;
import ru.t1.kharitonova.tm.marker.ServerCategory;
import ru.t1.kharitonova.tm.service.dto.ProjectDTOService;
import ru.t1.kharitonova.tm.service.dto.ProjectTaskDTOService;
import ru.t1.kharitonova.tm.service.dto.TaskDTOService;
import ru.t1.kharitonova.tm.service.dto.UserDTOService;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Category(ServerCategory.class)
public class ProjectTaskServiceDTOTest {

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    protected final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    private final IProjectDTOService projectService = new ProjectDTOService(connectionService);

    @NotNull
    private final ITaskDTOService taskService = new TaskDTOService(connectionService);

    @NotNull
    private final IProjectTaskDTOService projectTaskService = new ProjectTaskDTOService(connectionService, projectService, taskService);

    @NotNull
    private final IUserDTOService userService = new UserDTOService(connectionService, propertyService, projectService, taskService);

    @NotNull
    private List<ProjectDTO> projectDTOList;

    @NotNull
    private List<TaskDTO> taskDTOList;

    @NotNull
    private List<UserDTO> userDTOList;

    @NotNull
    private final String projectName = "Project Name Test";

    @NotNull
    private final String taskName = "Task Name Test";

    @NotNull
    private final String description = "Description";

    @Before
    public void init() {
        projectDTOList = new ArrayList<>();
        taskDTOList = new ArrayList<>();
        userDTOList = new ArrayList<>();

        if (userService.findByLogin("admin") == null) {
            @NotNull final UserDTO userDTOAdmin = new UserDTO();
            userDTOAdmin.setLogin("admin");
            userDTOAdmin.setPasswordHash("admin");
            userDTOAdmin.setEmail("admin@test.ru");
            userDTOAdmin.setRole(Role.ADMIN);
            userService.add(userDTOAdmin);
        }
        if (userService.findByLogin("test") == null) {
            @NotNull final UserDTO userDTOTest = new UserDTO();
            userDTOTest.setLogin("test");
            userDTOTest.setPasswordHash("test");
            userDTOTest.setEmail("test@test.ru");
            userDTOTest.setRole(Role.USUAL);
            userService.add(userDTOTest);
        }
        @Nullable UserDTO userDTOAdminBase = userService.findByLogin("admin");
        @Nullable UserDTO userDTOTestBase = userService.findByLogin("test");
        userDTOList.add(userDTOAdminBase);
        userDTOList.add(userDTOTestBase);
        @NotNull final String adminId = userDTOAdminBase.getId();
        @NotNull final String userId = userDTOTestBase.getId();

        projectDTOList.add(new ProjectDTO(adminId, "Test Project 1", "Description 1 admin", Status.IN_PROGRESS));
        projectDTOList.add(new ProjectDTO(adminId, "Test Project 2", "Description 2 admin", Status.NOT_STARTED));
        projectDTOList.add(new ProjectDTO(userId, "Test Project 3", "Description 3 user", Status.IN_PROGRESS));
        projectDTOList.add(new ProjectDTO(userId, "Test Project 4", "Description 4 user", Status.NOT_STARTED));

        taskDTOList.add(new TaskDTO(adminId, "Test Task 1", "Description 1 admin", Status.NOT_STARTED, projectDTOList.get(0).getId()));
        taskDTOList.add(new TaskDTO(adminId, "Test Task 2", "Description 2 admin", Status.IN_PROGRESS, projectDTOList.get(1).getId()));
        taskDTOList.add(new TaskDTO(userId, "Test Task 3", "Description 3 user", Status.IN_PROGRESS, projectDTOList.get(2).getId()));
        taskDTOList.add(new TaskDTO(userId, "Test Task 4", "Description 4 user", Status.IN_PROGRESS, null));

        taskService.removeAll();
        projectService.removeAll();
        for (@NotNull final ProjectDTO projectDTO : projectDTOList) projectService.add(projectDTO);
        for (@NotNull final TaskDTO taskDTO : taskDTOList) taskService.add(taskDTO);
    }

    @After
    public void clear() {
        taskService.removeAll();
        projectService.removeAll();
        taskDTOList.clear();
        userDTOList.clear();
    }

    @Test
    public void testBindTaskToProject() {
        @NotNull final String user_test_id = userDTOList.get(0).getId();
        @NotNull final ProjectDTO projectDTO = projectService.create(user_test_id, projectName, description);
        @NotNull final String projectId = projectDTO.getId();
        @Nullable TaskDTO taskDTO = taskService.create(user_test_id, taskName, description);
        @NotNull final String taskId = taskDTO.getId();

        projectTaskService.bindTaskToProject(user_test_id, projectId, taskId);
        @Nullable TaskDTO updatedTaskDTO = taskService.findOneById(user_test_id, taskId);
        Assert.assertNotNull(updatedTaskDTO);
        Assert.assertEquals(updatedTaskDTO.getProjectId(), projectId);

        Assert.assertThrows(
                UserIdEmptyException.class,
                () -> projectTaskService.bindTaskToProject("", projectId, taskId)
        );
        Assert.assertThrows(
                UserIdEmptyException.class,
                () -> projectTaskService.bindTaskToProject(null, projectId, taskId)
        );
        Assert.assertThrows(
                ProjectIdEmptyException.class,
                () -> projectTaskService.bindTaskToProject(user_test_id, "", taskId)
        );
        Assert.assertThrows(
                ProjectIdEmptyException.class,
                () -> projectTaskService.bindTaskToProject(user_test_id, null, taskId)
        );
        Assert.assertThrows(
                TaskIdEmptyException.class,
                () -> projectTaskService.bindTaskToProject(user_test_id, projectId, "")
        );
        Assert.assertThrows(
                TaskIdEmptyException.class,
                () -> projectTaskService.bindTaskToProject(user_test_id, projectId, null)
        );
        Assert.assertThrows(
                ProjectNotFoundException.class,
                () -> projectTaskService.bindTaskToProject(user_test_id, UUID.randomUUID().toString(), taskId)
        );
        Assert.assertThrows(
                TaskNotFoundException.class,
                () -> projectTaskService.bindTaskToProject(user_test_id, projectId, UUID.randomUUID().toString())
        );
    }

    @Test
    public void testRemoveProjectById() {
        @NotNull final String user_test_id = userDTOList.get(0).getId();
        @NotNull final ProjectDTO projectDTO = projectService.create(user_test_id, projectName, description);
        @NotNull final String projectId = projectDTO.getId();
        @NotNull final TaskDTO taskDTO = taskService.create(user_test_id, taskName, description);
        @NotNull final String taskId = taskDTO.getId();
        projectTaskService.bindTaskToProject(user_test_id, projectId, taskId);
        projectTaskService.removeProjectById(user_test_id, projectId);
        Assert.assertNull(projectService.findOneById(user_test_id, projectId));
        Assert.assertNull(taskService.findOneById(user_test_id, taskId));

        Assert.assertThrows(
                UserIdEmptyException.class,
                () -> projectTaskService.removeProjectById("", projectId)
        );
        Assert.assertThrows(
                UserIdEmptyException.class,
                () -> projectTaskService.removeProjectById(null, projectId)
        );
        Assert.assertThrows(
                ProjectIdEmptyException.class,
                () -> projectTaskService.removeProjectById(user_test_id, null)
        );
    }

    @Test
    public void testUnbindTaskFromProject() {
        @NotNull final String user_test_id = userDTOList.get(0).getId();
        @NotNull final ProjectDTO projectDTO = projectService.create(user_test_id, projectName, description);
        @NotNull final String projectId = projectDTO.getId();
        @NotNull final TaskDTO taskDTO = taskService.create(user_test_id, taskName, description);
        @NotNull final String taskId = taskDTO.getId();
        Assert.assertThrows(
                UserIdEmptyException.class,
                () -> projectTaskService.unbindTaskFromProject("", projectId, taskId)
        );
        Assert.assertThrows(
                UserIdEmptyException.class,
                () -> projectTaskService.unbindTaskFromProject(null, projectId, taskId)
        );
        Assert.assertThrows(
                ProjectIdEmptyException.class,
                () -> projectTaskService.unbindTaskFromProject(user_test_id, "", taskId)
        );
        Assert.assertThrows(
                ProjectIdEmptyException.class,
                () -> projectTaskService.unbindTaskFromProject(user_test_id, null, taskId)
        );
        Assert.assertThrows(
                TaskIdEmptyException.class,
                () -> projectTaskService.unbindTaskFromProject(user_test_id, projectId, "")
        );
        Assert.assertThrows(
                TaskIdEmptyException.class,
                () -> projectTaskService.unbindTaskFromProject(user_test_id, projectId, null)
        );
        Assert.assertThrows(
                ProjectNotFoundException.class,
                () -> projectTaskService.unbindTaskFromProject(user_test_id, UUID.randomUUID().toString(), taskId)
        );
        Assert.assertThrows(
                TaskNotFoundException.class,
                () -> projectTaskService.unbindTaskFromProject(user_test_id, projectId, UUID.randomUUID().toString())
        );
        projectTaskService.unbindTaskFromProject(user_test_id, projectId, taskId);
        Assert.assertNotNull(taskService.findOneById(user_test_id, taskId));
        Assert.assertNull(taskService.findOneById(user_test_id, taskId).getProjectId());
        Assert.assertNull(taskDTO.getProjectId());
    }

}
