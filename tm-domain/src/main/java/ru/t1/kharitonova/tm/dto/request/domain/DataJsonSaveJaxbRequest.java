package ru.t1.kharitonova.tm.dto.request.domain;

import org.jetbrains.annotations.Nullable;
import ru.t1.kharitonova.tm.dto.request.AbstractUserRequest;

public final class DataJsonSaveJaxbRequest extends AbstractUserRequest {

    public DataJsonSaveJaxbRequest(@Nullable final String token) {
        super(token);
    }

}
