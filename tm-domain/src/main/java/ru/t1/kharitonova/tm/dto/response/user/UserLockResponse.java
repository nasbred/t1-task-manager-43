package ru.t1.kharitonova.tm.dto.response.user;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;
import ru.t1.kharitonova.tm.dto.response.AbstractUserResponse;
import ru.t1.kharitonova.tm.dto.model.UserDTO;

@NoArgsConstructor
public final class UserLockResponse extends AbstractUserResponse {

    public UserLockResponse(@Nullable final UserDTO userDTO) {
        super(userDTO);
    }

    public UserLockResponse(@Nullable final String token) {
        super(token);
    }

}
