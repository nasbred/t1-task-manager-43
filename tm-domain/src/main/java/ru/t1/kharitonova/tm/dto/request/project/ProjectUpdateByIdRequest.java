package ru.t1.kharitonova.tm.dto.request.project;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.kharitonova.tm.dto.request.AbstractUserRequest;

@Getter
@Setter
@NoArgsConstructor
public final class ProjectUpdateByIdRequest extends AbstractUserRequest {

    @Nullable
    private String id;

    @Nullable
    private String name;

    @Nullable
    private String description;

    public ProjectUpdateByIdRequest(@Nullable final String token) {
        super(token);
    }

    public ProjectUpdateByIdRequest(@Nullable String token, @Nullable String id, @Nullable String name, @Nullable String description) {
        super(token);
        this.id = id;
        this.name = name;
        this.description = description;
    }

}
