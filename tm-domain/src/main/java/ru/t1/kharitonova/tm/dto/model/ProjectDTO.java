package ru.t1.kharitonova.tm.dto.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.kharitonova.tm.api.model.IWBS;
import ru.t1.kharitonova.tm.enumerated.Status;

import javax.persistence.*;
import java.util.Date;
import java.util.Objects;

@Getter
@Setter
@Entity
@Table(name = "tm_project")
@NoArgsConstructor
public final class ProjectDTO extends AbstractUserOwnedModelDTO implements IWBS {

    @NotNull
    @Column(name = "name", nullable = false)
    private String name = "";

    @Nullable
    @Column(name = "description")
    private String description = "";

    @NotNull
    @Column(name = "status", nullable = false)
    @Enumerated(EnumType.STRING)
    private Status status = Status.NOT_STARTED;

    @NotNull
    @Column(name = "created", nullable = false)
    private Date created = new Date();

    public ProjectDTO(@NotNull final String name, @NotNull final Status status) {
        setName(name);
        setStatus(status);
    }

    public ProjectDTO(@NotNull final String userId, @NotNull final String name, @Nullable final String description, @NotNull final Status status) {
        super(userId);
        this.name = name;
        this.description = description;
        this.status = status;
    }

    @NotNull
    @Override
    public String toString() {
        return name + " : " + description;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ProjectDTO projectDTO = (ProjectDTO) o;
        return name.equals(projectDTO.name) &&
                Objects.equals(description, projectDTO.description) &&
                status == projectDTO.status &&
                created.equals(projectDTO.created);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, description, status, created);
    }

}
