package ru.t1.kharitonova.tm.dto.response;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.kharitonova.tm.dto.model.UserDTO;

@Getter
@Setter
@NoArgsConstructor
public abstract class AbstractUserResponse extends AbstractResultResponse {

    @Nullable
    protected UserDTO userDTO;

    @Nullable
    protected String token;

    public AbstractUserResponse(@Nullable final UserDTO userDTO) {
        this.userDTO = userDTO;
    }

    public AbstractUserResponse(@Nullable String token) {
        this.token = token;
    }

    public AbstractUserResponse(@NotNull final Throwable throwable) {
        super(throwable);
    }

}